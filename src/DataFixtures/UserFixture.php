<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('dany.corbineau@dany.corbineau.fr');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setPassword($this->passwordEncoder->encodePassword($user,  $_ENV['ADMIN_PASSWORD']));
        $user->setName('Dany');
        $manager->persist($user);
        $user = new User();
        $user->setEmail($_ENV['M_USER']);
        $user->setRoles(['ROLE_USER']);
        $user->setPassword($this->passwordEncoder->encodePassword($user, $_ENV['M_PASSWORD']));
        $user->setName($_ENV['M_NAME']);
        $manager->persist($user);
        $manager->flush();
    }
}
