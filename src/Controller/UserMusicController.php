<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Music;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class UserMusicController extends AbstractController
{
    /**
     * @Route("/user/music/{id}/{playlist}", name="user_music", requirements={"playlist"="\d+"})
     */
    public function index($id, $playlist = 0)
    {
        $repository = $this->getDoctrine()->getRepository(Music::class);
        /** @var Music $music */
        $music = $repository->find($id);
        return $this->render('user_music/index.html.twig', [
            'music' => $music,
            'return_playlist' => $playlist
        ]);
    }

    /**
     *
     * @Route("/user/music/{id}/{playlist}/comment", name="user_music_comment", requirements={"playlist"="\d+"})
     * @param $id
     */
    public function comment($id, $playlist = 0, Security $security, Request $request)
    {
        $user = $security->getUser();
        $repository = $this->getDoctrine()->getRepository(Music::class);
        /** @var Music $music */
        $music = $repository->find($id);
        $comment = new Comment();
        $comment->setDate(new \DateTime());
        $comment->setVideo($music);
        $comment->setUser($user);
        $comment->setText(htmlspecialchars($request->request->get('comment')));
        $em = $this->getDoctrine()->getManager();
        $em->persist($comment);
        $em->flush();
        return $this->redirectToRoute('user_music', ['id' => $id, 'playlist' => $playlist]);
    }
}
