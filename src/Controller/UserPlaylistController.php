<?php

namespace App\Controller;

use App\Entity\Music;
use App\Entity\Playlist;
use App\Entity\Released;
use DateTime;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class UserPlaylistController extends AbstractController
{
    /**
     * @Route("/user/playlist", name="user_playlist")
     */
    public function index()
    {
        return $this->redirectToRoute('user_playlist_list');
    }

    /**
     * @Route("/user/playlist/list", name="user_playlist_list")
     * @param Security $security
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function playlistList(Security $security)
    {
        $user = $security->getUser();
        $repository = $this->getDoctrine()->getRepository(Playlist::class);
        $playlists = $repository->findBy(['user' => $user]);
        $datas = [];
        /** @var Playlist $playlist */
        foreach ($playlists as $playlist) {
            $data = [
                'name' => $playlist->getName(),
                'music_nb' => $playlist->countMusicReleased(),
                'link' => $this->generateUrl('user_playlist_show', ['id' => $playlist->getId()])
            ];
            $datas[] = $data;
        }

        return $this->render('user_playlist/list.html.twig', [
            'playlists' => $datas
        ]);
    }

    /**
     * @Route("/user/playlist/{id}/show", name="user_playlist_show")
     * @param int $id
     */
    public function show(int $id)
    {
        $repository = $this->getDoctrine()->getRepository(Playlist::class);
        /** @var Playlist $playlist */
        $playlist = $repository->find($id);
        $releseds = $playlist->getReleases()->toArray();
        usort($releseds, function ($a, $b) {
            return $a->getDate() < $b->getDate();
        });
        return $this->render('user_playlist/show.html.twig', [
            'playlist' => $playlist,
            'releaseds' => $releseds,
            'canGenerate' => $playlist->canGenerate()
        ]);
    }

    /**
     * @Route("/user/playlist/{id}/generateRelease", name="user_playlist_generate_release")
     * @param $id
     * @throws Exception
     */
    public function generateRelease($id, Security $security)
    {
        $repository = $this->getDoctrine()->getRepository(Playlist::class);
        /** @var Playlist $playlist */
        $playlist = $repository->find($id);

        if($playlist->canGenerate()) {
            $musicRepository = $this->getDoctrine()->getRepository(Music::class);
            /** @var Music[] $musics */
            $musics = $musicRepository->findAll();
            $musicAlreadyReleased = [];
            $newRelease = new Released();
            $newRelease->setDate(new DateTime());
            $newRelease->setPlaylist($playlist);
            /** @var Released $released */
            foreach ($playlist->getReleases()->toArray() as $released) {
                $musicAlreadyReleased = array_merge($musicAlreadyReleased, $released->getMusics()->toArray());
            }

            if(count($musics) > count($musicAlreadyReleased)+4)
            {
                $videoAdded = 0;
                while ($videoAdded < 5) {
                    $musicId = random_int(1, count($musics));
                    $isAdded = false;
                    if(array_key_exists($musicId, $musics)) {
                        /** @var Music $musicAR */
                        foreach ($musicAlreadyReleased as $musicAR) {
                            if($musicAR->getId() === $musics[$musicId]->getId()) {
                                $isAdded = true;
                                break;
                            }
                        }
                        if(!$isAdded) {
                            ++$videoAdded;
                            $newRelease->addMusic($musics[$musicId]);
                            $musicAlreadyReleased[] = $musics[$musicId];
                        }
                    }
                }
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($newRelease);
                $entityManager->flush();
            }
        }
        return $this->redirectToRoute('user_playlist_show', ['id' => $id]);
    }
}
