<?php

namespace App\Repository;

use App\Entity\Released;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Released|null find($id, $lockMode = null, $lockVersion = null)
 * @method Released|null findOneBy(array $criteria, array $orderBy = null)
 * @method Released[]    findAll()
 * @method Released[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReleasedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Released::class);
    }

    // /**
    //  * @return Release[] Returns an array of Release objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Release
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
