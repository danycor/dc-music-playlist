<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlaylistRepository")
 */
class Playlist
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="playlists")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="Released", mappedBy="playlist", orphanRemoval=true)
     */
    private $releases;

    public function __construct()
    {
        $this->releases = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Released[]
     */
    public function getReleases(): Collection
    {
        return $this->releases;
    }

    public function addRelease(Released $release): self
    {
        if (!$this->releases->contains($release)) {
            $this->releases[] = $release;
            $release->setPlaylist($this);
        }

        return $this;
    }

    public function removeRelease(Released $release): self
    {
        if ($this->releases->contains($release)) {
            $this->releases->removeElement($release);
            // set the owning side to null (unless already changed)
            if ($release->getPlaylist() === $this) {
                $release->setPlaylist(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function canGenerate()
    {
        $current_date = new DateTime();
        /** @var Released $release */
        foreach ($this->releases->toArray() as $release) {
            if($release->getDate()->format('Y-m-d') == $current_date->format('Y-m-d')) {
                return false;
            }
        }
        return true;
    }

    public function countMusicReleased()
    {
        $nb = 0;
        /** @var Released $released */
        foreach ($this->getReleases()->toArray() as $released) {
            $nb += $released->getMusics()->count();
        }
        return $nb;
    }
}
