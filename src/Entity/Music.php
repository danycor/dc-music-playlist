<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MusicRepository")
 */
class Music
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $youtubeLink;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="video", orphanRemoval=true)
     */
    private $comments;

    /**
     * @ORM\ManyToMany(targetEntity="Released", mappedBy="musics")
     */
    private $releases;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="musics")
     */
    private $category;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->releases = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getYoutubeLink(): ?string
    {
        return $this->youtubeLink;
    }

    public function setYoutubeLink(string $youtubeLink): self
    {
        $this->youtubeLink = $youtubeLink;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setVideo($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getVideo() === $this) {
                $comment->setVideo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Released[]
     */
    public function getReleases(): Collection
    {
        return $this->releases;
    }

    public function addRelease(Released $release): self
    {
        if (!$this->releases->contains($release)) {
            $this->releases[] = $release;
            $release->addMusic($this);
        }

        return $this;
    }

    public function removeRelease(Released $release): self
    {
        if ($this->releases->contains($release)) {
            $this->releases->removeElement($release);
            $release->removeMusic($this);
        }

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getVideoId()
    {
        return substr($this->getYoutubeLink(), strpos($this->getYoutubeLink(), "?v=")+3);
    }
}
