git pull
docker-compose down
docker-compose build
docker-compose up -d
sleep 5
docker exec -it dcmusicplaylist-php "php bin/console doctrine:database:create"
docker exec -it dcmusicplaylist-php "php bin/console make:migration"
docker exec -it dcmusicplaylist-php "php bin/console doctrine:migration:migrate"
