# Playlist

## CMD
* Make DB `php bin/console doctrine:database:create`
* Make Migration `php bin/console make:migration`
* Make Migration in DB `php bin/console doctrine:migrations:migrate`
* RUN server in local dev : `php bin/console server:run`
* Load fixture `php bin/console doctrine:fixtures:load`
* Cache clear `php bin/console cache:clear`
* Installation des assets `bin/console assets:install`

* Créer des fixtures : `php bin/console make:fixtures`